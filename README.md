# Toolbar Edit Page Button

Toolbar Edit Page Button adds a button (also known as a link or a
toolbar tab) to edit the currently viewed node in the admin toolbar,
at the top of the site. The button is visible on all nodes, to users
with permission to access the toolbar edit page button. Clicking the
button opens the current node in edit mode.

The module uses the same layout (for text and placement) as WordPress’
edit button. This is where a lot of editors are used to and expect to
find a button to edit the current page. The module doesn’t remove tabs
or the Quick edit feature. Some users are not familiar with the term
node which is why it is labeled ‘Edit Page’ instead of ‘Edit Node’.


For a full description of the module, visit the
[project page](https://www.drupal.org/project/toolbar_edit_page_button).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/toolbar_edit_page_button).


## Node-id

On the button, next to the Edit Page link, the node id (nid) is displayed
within parentheses so editors easily see which node (or page) they are
about to edit.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

No configuration needed.


## Theming

It is possible to style the background color if you want the button to pop 
more:

        a[href^="/node/"].toolbar-icon-edit.toolbar-item {
          background-color: #006600;
        }


## Similar modules

[Toolbar Edit Button](https://www.drupal.org/project/toolbar_edit_button): 
Adding the node edit (and taxonomy term edit) button into the admin toolbar. 
This module have a different placing of the button, no nid and also removes 
quick edit.
